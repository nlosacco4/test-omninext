Lambda function

With a lambda function you can create a REST API, which is an API
 (REpresentational State Transfer - Application
Programming Interface ) endpoint that can accept HTTP web requests to
 execute Create Read Update and Delete operations server side on DB.

It is serverless : it means you have not responsability to maintain the 
server ( hardware and configuration setting files ).

For this reason there are any objects you need to configure :
-policy ( using this one you can establish a secure connection)
- role ( whith which you can set permission to user to file system )

These two objects abilitate tour access to the cloud server.

Since you can access to data using the Http requests you have to define
the meta functions ( and the meta objects - in this case of the json type )
to let your serverless server accept input data.

In this point it is usefull to ping the endpoint to test if it is reachable
( 200 http response is an OK reponse )

From the console of the provider you can create at this point endpoint 
( which is the API ) and the resource ( for example the DynamoDBManager
which is an object that makes transparent to you all the settings for 
Database connection ).

At this point you can create a POST method ( which represents POST HTTP 
request ) and bind it to the LambdaFunctionOverHttps.

At this point before you can store the information you send through web you
need to create a table on the Database specifying only the type of data you
want to store

After you' ve made all these operations you can deploy the serverless server.

Because it is useful to deploy your endpoints most of the times through
different stages ( which can be of TEST , Quality Assurance an Production )
you need to define a stage in which you want to deploy your web service.

At this point you can finally any HTTP requests to do some operations on data.

This is an example:

curl <endpoint-url> \
-d '{"operation": "<operation you want to do (e.g delete)>", "payload": {"Key": {"id": "<id_value>"}}}'

In this case you can use only the Key since it is a delete operation.
If it was for example a create operation you need also to insert a value 
parameter in the json object.

--continuare con k8s and docker

